docker-reload:
	docker-compose down && docker-compose build --no-cache && docker-compose up

deploy:
	eval $(docker-machine env travelbeaverdev) && docker-compose down && docker-compose build && docker-compose up -d

deploy-attached:
	eval $(docker-machine env travelbeaverdev) && docker-compose down && docker-compose build && docker-compose up