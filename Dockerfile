FROM python:3.4-alpine

# Supercronic

# from https://github.com/aptible/supercronic/releases
ENV SUPERCRONIC_URL=https://github.com/aptible/supercronic/releases/download/v0.1.2/supercronic-linux-amd64 \
    SUPERCRONIC=supercronic-linux-amd64 \
    SUPERCRONIC_SHA1SUM=cdfde14f50a171cbfc35a3a10429e2ab0709afe0

ENTRYPOINT [ "/sbin/tini", "--" ]

# install dependencies
RUN apk add --no-cache \
        ca-certificates \
        curl \
        tini \

# install supercronic
# (from https://github.com/aptible/supercronic/releases)
 && curl -fsSLO "$SUPERCRONIC_URL" \
 && echo "${SUPERCRONIC_SHA1SUM}  ${SUPERCRONIC}" | sha1sum -c - \
 && chmod +x "$SUPERCRONIC" \
 && mv "$SUPERCRONIC" "/usr/local/bin/${SUPERCRONIC}" \
 && ln -s "/usr/local/bin/${SUPERCRONIC}" /usr/local/bin/supercronic

ADD crontab /etc/crontab

# Application
ADD ./app/ /code
ADD ./requirements.txt /code
WORKDIR /code

# Pillow deps
RUN apk update
RUN apk add build-base python-dev py-pip jpeg-dev zlib-dev
ENV LIBRARY_PATH=/lib:/usr/lib

RUN pip install -r requirements.txt

# Run app only
# CMD [ "python", "/code/app.py" ]

# Run supercronic
CMD [ "/usr/local/bin/supercronic", "/etc/crontab" ]