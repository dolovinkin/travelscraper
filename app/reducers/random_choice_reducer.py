import random


class RandomChoiceReducer:
    def reduce(self, data):
        random.shuffle(data)
        return random.choice(data)
