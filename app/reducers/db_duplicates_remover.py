from db import Database


class DbDuplicatesRemover:
    def __init__(self):
        self.db = Database()

    def reduce(self, data):
        result = []
        for tour in data:
            if not self.db.tour_exists(tour):
                result.append(tour)
        return result
