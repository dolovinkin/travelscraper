class FirstNReducer:
    def __init__(self, N):
        self.N = N

    def reduce(self, data):
        return data[0:self.N]