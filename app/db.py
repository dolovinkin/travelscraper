import sqlite3


RECENT_TOURS_LENGTH = 10


class Database:
    def __init__(self, filename='tours.sqlite3'):
        self.conn = sqlite3.connect(filename)
        if not self._table_exists('tours'):
            self._init_db_struct()

    def _init_db_struct(self):
        c = self.conn.cursor()
        c.execute('''
            CREATE TABLE tours(
                id TEXT PRIMARY KEY NOT NULL,
                destination TEXT NOT NULL,
                created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
            )
        ''')

    def _table_exists(self, table_name):
        c = self.conn.cursor()
        c.execute('''
            SELECT name FROM sqlite_master WHERE type='table' AND name=?;
        ''', (table_name,))
        return len(c.fetchall()) > 0

    def post_tour(self, tour):
        c = self.conn.cursor()
        c.execute('''
            INSERT INTO tours VALUES(?, ?, CURRENT_TIMESTAMP)
        ''', (tour['id'], tour['destination']))
        self.conn.commit()

    def tour_exists(self, tour):
        c = self.conn.cursor()
        c.execute('''
            SELECT id FROM tours WHERE id = ?
        ''', (tour['id'],))
        return len(c.fetchall()) > 0

    def get_recent_countries(self, limit=RECENT_TOURS_LENGTH):
        c = self.conn.cursor()
        c.execute('''
            SELECT destination
            FROM tours
            ORDER BY created_at DESC
            LIMIT {}
        '''.format(limit))
        countries = c.fetchall()
        result = {}
        for country in countries:
            country = country[0]
            if country in result:
                result[country] += 1
            else:
                result[country] = 1
        print(result)
        return result
