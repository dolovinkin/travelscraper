def cmp_to_key(mycmp):
    'Convert a cmp= function into a key= function'
    class K:
        def __init__(self, obj, *args):
            self.obj = obj
        def __lt__(self, other):
            return mycmp(self.obj, other.obj) < 0
        def __gt__(self, other):
            return mycmp(self.obj, other.obj) > 0
        def __eq__(self, other):
            return mycmp(self.obj, other.obj) == 0
        def __le__(self, other):
            return mycmp(self.obj, other.obj) <= 0
        def __ge__(self, other):
            return mycmp(self.obj, other.obj) >= 0
        def __ne__(self, other):
            return mycmp(self.obj, other.obj) != 0
    return K


class BaseSorter:
    def __init__(self, reverse=False):
        self.reverse = reverse

    def _cmp(self):
        raise NotImplementedError('Comparator function not implemented')

    def sort(self, tours):
        return sorted(tours, key=cmp_to_key(self._cmp), reverse=self.reverse)
