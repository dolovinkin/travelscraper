from .common import BaseSorter


class PriceDaySorter(BaseSorter):
    def _cmp(self, tour_a, tour_b):
        price_day_a = tour_a['price'] / tour_a['nights']
        price_day_b = tour_b['price'] / tour_b['nights']
        if price_day_a > price_day_b:
            return 1
        elif price_day_a == price_day_b:
            return 0
        else:
            return -1
