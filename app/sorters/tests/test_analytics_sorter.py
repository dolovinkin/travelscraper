from random import shuffle
from countries import COUNTRIES
from sorters.analytics_sorter import AnalyticsSorter


def test_sort():
    min_price_day = {
        'pansion': 'BB',
        'destination': COUNTRIES.THAILAND,
        'price': 9000,
        'nights': 3,
        'stars': 2,
    }
    average_price_day = {
        'pansion': 'HB',
        'destination': COUNTRIES.ITALY,
        'price': 16000,
        'nights': 3,
        'stars': 3,
    }
    max_price_day = {
        'pansion': 'AI+',
        'destination': COUNTRIES.GREECE,
        'price': 30000,
        'nights': 1,
        'stars': 1,
    }
    tours = [min_price_day, average_price_day, max_price_day]
    shuffle(tours)
    tours = AnalyticsSorter().sort(tours)
    print('test')
    assert isinstance(tours, list), True
