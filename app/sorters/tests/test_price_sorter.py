from random import shuffle
from sorters.price_sorter import PriceSorter


def test_sort():
    min_price_day = {
        'price': 30,
        'nights': 3,
    }
    average_price_day = {
        'price': 500,
        'nights': 3,
    }
    max_price_day = {
        'price': 30000,
        'nights': 1,
    }
    tours = [min_price_day, average_price_day, max_price_day]
    shuffle(tours)
    tours = PriceSorter().sort(tours)
    assert min_price_day == tours[0]
    assert max_price_day == tours[-1]
