from db import Database
import re
from .common import BaseSorter
from countries import COUNTRIES


COUNTRY_WEIGHT = 1.5
PRICE_UNDER_WEIGHT = 3
ACCOMODATION_WEIGHT = 1
DURATION_WEIGHT = 1.3
STARS_WEIGHT = 0.4

COUNTRIES_RANK = {
    COUNTRIES.BULGARIA: 0.5,
    COUNTRIES.CYPRIS: 0.5,
    COUNTRIES.CZECH_REPUBLIC: 0.8,
    COUNTRIES.GREECE: 0.5,
    COUNTRIES.ITALY: 0.8,
    COUNTRIES.MONTENEGRO: 0.6,
    COUNTRIES.LATVIA: 0.7,
    COUNTRIES.SEYCHELLES: 1,
    COUNTRIES.SPAIN: 0.7,
    COUNTRIES.THAILAND: 0.5,
    COUNTRIES.TUNISIA: 0.5,
    COUNTRIES.TURKEY: 0.0,
    COUNTRIES.UAE: 0.5,
    COUNTRIES.VIETNAM: 0.7,
    COUNTRIES.SRI_LANKA: 1,
}

PRICE_UNDER_RANK = (
    (6000, 1.3),
    (11000, 1),
    (16000, 0.8),
    (22000, 0.6),
    (27000, 0.3),
    (33000, 0.2),
    (38000, 0.1),
    (1000000, 0.05)
)

ACCOMODATION_RANK = {
    'UAI': 1.1,
    'AI': 1,
    'FB+': 0.95,
    'FB': 0.9,
    'HB+': 0.85,
    'HB': 0.8,
    'BB': 0.7,
    'RO': 0.6
}

DURATION_UNDER_RANK = (
    (4, 0.3),
    (8, 0.7),
    (15, 0.9),
    (100, 1),
)


STARS_UNDER_RANK = (
    (0, 0.1),
    (1, 0.2),
    (2, 0.3),
    (3, 0.5),
    (4, 0.8),
    (5, 1),
)


class AnalyticsSorter(BaseSorter):
    def __init__(self, reverse=False):
        self.reverse = reverse
        self._calc_countries_bias(Database().get_recent_countries())

    def _calc_countries_bias(self, countries):
        for country, encounters in countries.items():
            COUNTRIES_RANK[country] -= 0.2 * encounters

    def _calc_rating(self, tour):
        total_score = 0

        # Price
        price_per_person = tour['price'] / 2
        for price, rank in PRICE_UNDER_RANK:
            if price_per_person <= price:
                total_score += rank * PRICE_UNDER_WEIGHT
                tour['price_under_cat'] = price
                break

        # Duration
        tour_duration = tour['nights']
        for duration, rank in DURATION_UNDER_RANK:
            if tour_duration <= duration:
                total_score += rank * DURATION_WEIGHT
                break

        # Accomodation
        for name, acc_score in ACCOMODATION_RANK.items():
            if re.search(name, tour['pansion']) is not None:
                total_score += acc_score * ACCOMODATION_WEIGHT
                break

        # Country
        total_score += COUNTRIES_RANK[tour['destination']] * COUNTRY_WEIGHT

        # Stars
        tour_stars = tour['stars']
        for stars, rank in STARS_UNDER_RANK:
            if tour_stars <= stars:
                total_score += rank * STARS_WEIGHT
                break

        tour['score'] = total_score
        return total_score

    def _cmp(self, tour_a, tour_b):
        tour_a_score = self._calc_rating(tour_a)
        tour_b_score = self._calc_rating(tour_b)
        if tour_a_score < tour_b_score:
            return 1
        elif tour_a_score == tour_b_score:
            return 0
        else:
            return -1
