from .common import BaseSorter


class PriceSorter(BaseSorter):
    def _cmp(self, tour_a, tour_b):
        if tour_a['price'] > tour_b['price']:
            return 1
        elif tour_a['price'] == tour_b['price']:
            return 0
        else:
            return -1
