from db import Database
import os
from countries import COUNTRIES


def test_recent_tours():
    if os.path.exists("test.sqlite3"):
        os.remove("test.sqlite3")
    database = Database("test.sqlite3")
    database.post_tour({
        'id': '1',
        'destination': COUNTRIES.THAILAND,
    })
    database.post_tour({
        'id': '2',
        'destination': COUNTRIES.THAILAND,
    })
    database.post_tour({
        'id': '3',
        'destination': COUNTRIES.CYPRIS,
    })
    database.post_tour({
        'id': '4',
        'destination': COUNTRIES.THAILAND,
    })
    tours = database.get_recent_countries()
    assert tours[COUNTRIES.CYPRIS] == 1
    assert tours[COUNTRIES.THAILAND] == 3
