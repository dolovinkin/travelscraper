import telegram


class TelegramChannelPoster:
    def __init__(self, chat_id, bot_token):
        self.chat_id = chat_id
        self.bot = telegram.Bot(bot_token)

    def post(self, obj):
        self.bot.sendMessage(
            chat_id=self.chat_id,
            parse_mode='HTML',
            text=obj['msg'],
            reply_markup=telegram.InlineKeyboardMarkup([[
                telegram.InlineKeyboardButton(
                    text="Посмотреть",
                    url=obj['url']
                )
            ]])
        )
