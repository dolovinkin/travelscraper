import os
from random import shuffle
from countries import COUNTRIES
from posters import DbPoster
from db import Database


def test_post():
    if os.path.exists("tours.sqlite3"):
        os.remove("tours.sqlite3")
    test_tour = {
        'id': '123',
        'destination': COUNTRIES.THAILAND,
        'price': 30,
        'nights': 3,
    }

    DbPoster().post(test_tour)
    db = Database()
    assert db.tour_exists(test_tour) is True
