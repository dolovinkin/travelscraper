import base64
import io
import random
from PIL import Image
from imgurpython import ImgurClient


IMGUR_CLIENT_ID = '95ade27dc609fe9'
IMGUR_CLIENT_SECRET = 'dc72d85ae0285395c80696e38330f7946f2a1be6'

class CollageGeneratorProcessor:
    def process(self, obj):
        collage = Image.new("RGB", (785, 780), (255, 255, 255))
        PICS_COUNT = min(len(obj['pics']), 3)
        if PICS_COUNT != 3:
            return obj
        for i, pic_url in enumerate(obj['pics']):
            if i >= PICS_COUNT:
                break
            if 'base64' in pic_url:
                pic_url = pic_url.split('base64,')[1]
                pic_bytes = base64.b64decode(pic_url)
                pic_bytes = io.BytesIO(pic_bytes)
                if PICS_COUNT == 3:
                    pic = Image.open(pic_bytes)
                    pic = pic.resize((800, 800), Image.ANTIALIAS)
                    pic_width, pic_height = pic.size

                    # First image
                    if i == 0:
                        pic = pic.crop((
                            pic_width/2 - 370/2,
                            pic_height/2 - 760/2,
                            580,
                            760
                        ))
                        collage.paste(pic, (20, 20))
                    elif i == 1:
                        pic = pic.crop((
                            pic_width/2 - 370/2,
                            pic_height/2 - 395/2,
                            570,
                            560
                        ))
                        collage.paste(pic, (20 + 20 + 370, 20))
                    elif i == 2:
                        pic = pic.crop((
                            pic_width/2 - 370/2,
                            pic_height/2 - 395/2,
                            570,
                            560
                        ))
                        collage.paste(pic, (20 + 20 + 370, 20 + 3 + 380))
        collage_path = './' + obj['id'] + '.jpg'
        collage.save(collage_path)
        obj['pic'] = collage_path

        try:
            client = ImgurClient(IMGUR_CLIENT_ID, IMGUR_CLIENT_SECRET)
            obj['pic'] = client.upload_from_path(obj['pic'])['link']
        except:
            obj.pop('pic')
        return obj