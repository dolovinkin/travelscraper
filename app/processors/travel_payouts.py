import urllib


class TravelPayoutsProcessor:
    def __init__(self, sh_marker):
        self.sh_marker = sh_marker

    def process(self, obj):
        URL_TEMPLATE = "http://c26.travelpayouts.com/click?shmarker={sh_marker}&promo_id={promo_id}&source_type=customlink&type=click&custom_url={custom_url}"
        if 'level.travel' in obj['url']:
            promo_id = 660
        obj['url'] = URL_TEMPLATE.format(
            sh_marker=self.sh_marker,
            promo_id=promo_id,
            custom_url=urllib.parse.quote(obj['url'])
        )
        return obj