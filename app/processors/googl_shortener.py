from pyshorteners import Shortener


class GooglShortenerProcessor:
    def __init__(self, api_key):
        self.shortener = Shortener('Google', api_key=api_key)

    def process(self, obj):
        obj['url'] = self.shortener.short(obj['url'])
        return obj