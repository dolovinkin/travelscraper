import hashlib


class IdGeneratorProcessor:
    def process(self, obj):
        m = hashlib.md5()
        m.update(obj['url'].encode('utf-8'))
        obj['id'] = m.hexdigest()
        return obj

    def reduce(self, tours):
        result = list(tours)
        for tour in tours:
            result.append(self.process(tour))
        return result
