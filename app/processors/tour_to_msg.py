import pymorphy2
import re
from datetime import timedelta
from countries import COUNTRIES


VISA_VARIANTS = {
    'National/Schengen': 'Нужна, подойдет Шенген',
    'Schengen': 'Нужна, подойдет Шенген',
    'National': 'Нужна',
    'No': 'Не нужна',
}


COUNTRIES_MAP = {
    COUNTRIES.BULGARIA: 'Болгария',
    COUNTRIES.CYPRIS: 'Кипр',
    COUNTRIES.CZECH_REPUBLIC: 'Чехия',
    COUNTRIES.FRANCE: 'Франция',
    COUNTRIES.GERMANY: 'Германия',
    COUNTRIES.ISRAEL: 'Израиль',
    COUNTRIES.ITALY: 'Италия',
    COUNTRIES.MONTENEGRO: 'Черногория',
    COUNTRIES.LATVIA: 'Латвия',
    COUNTRIES.SEYCHELLES: 'Сейшелы',
    COUNTRIES.SPAIN: 'Испания',
    COUNTRIES.SRI_LANKA: 'Шри-Ланка',
    COUNTRIES.THAILAND: 'Тайланд',
    COUNTRIES.TUNISIA: 'Тунис',
    COUNTRIES.TURKEY: 'Турция',
    COUNTRIES.UAE: 'Арабские Эмираты',
    COUNTRIES.VIETNAM: 'Вьетнам',
}

VISA_MAP = {
    COUNTRIES.BULGARIA: 'National/Schengen',
    COUNTRIES.CYPRIS: 'No',
    COUNTRIES.CZECH_REPUBLIC: 'Schengen',
    COUNTRIES.FRANCE: 'Schengen',
    COUNTRIES.GERMANY: 'Schengen',
    COUNTRIES.ISRAEL: 'No',
    COUNTRIES.ITALY: 'Schengen',
    COUNTRIES.MONTENEGRO: 'No',
    COUNTRIES.LATVIA: 'Schengen',
    COUNTRIES.SEYCHELLES: 'No',
    COUNTRIES.SRI_LANKA: 'No',
    COUNTRIES.SPAIN: 'Schengen',
    COUNTRIES.THAILAND: 'No',
    COUNTRIES.TUNISIA: 'No',
    COUNTRIES.TURKEY: 'No',
    COUNTRIES.UAE: 'No',
    COUNTRIES.VIETNAM: 'No',
}

PANSION_MAP = {
    'RO': 'Без питания',
    'BB': 'Завтраки',
    'HB': 'Завтраки и ужины',
    'FB': 'Завтраки, обеды и ужины',
    'AI': 'Всё включено'
}


class TourToMsgProcessor:
    def __init__(self):
        self.morph = pymorphy2.MorphAnalyzer()

    def process(self, obj):
        MSG_TEMPLATE = (
        """
🌎 {country_name}{city_name} <a href="{pic}">&#8206;</a>
🏨 {hotel_name}{stars}, {room_name}
🍳 {pansion} ({pansion_code})
📇 {visa_info}
📅 {duration}, {departure} - {arrival}
💰 {price}₽ с человека (тур на двоих)
        """)

        # Город
        if obj['city'] is not None and obj['city'] != '':
            city_name = ', {}'.format(obj['city'])
        else:
            city_name = ''

        # Звёздочки
        if obj['stars'] >= 2:
            stars_txt = " {}*".format(str(obj['stars']))
        else:
            stars_txt = ""
        
        # Склонение и согласование с числительным слова "ночи"
        noch = self.morph.parse("ночь")[0]
        noches = self.morph.parse(noch.make_agree_with_number(obj['nights']).word)[0].inflect({'gent'}).word
        duration = '{} {}'.format(obj['nights'], noches)
        

        # Вылет - прилёт
        departure = obj['departure']
        arrival = departure + timedelta(days=obj['nights'])
        departure = departure.strftime('%d.%m')
        arrival = arrival.strftime('%d.%m')

        # Картинка
        if 'pic' in obj:
            pic = obj['pic']
        else:
            pic = '#'

        # Пансион
        pansion = None
        for name, readable_name in PANSION_MAP.items():
            if re.search(name, obj['pansion']) is not None:
                pansion = readable_name
                break


        # Информация о визе
        visa_info = "Виза {}".format((VISA_VARIANTS[VISA_MAP[obj['destination']]]).lower())

        obj['msg'] = MSG_TEMPLATE.format(
            country_name=COUNTRIES_MAP[obj['destination']],
            city_name=city_name,
            duration=duration,
            hotel_name=obj['hotel_name'],
            stars=stars_txt,
            room_name=obj['room_name'].lower(),
            pansion=PANSION_MAP[obj['pansion']],
            pansion_code=obj['pansion'],
            departure=departure,
            arrival=arrival,
            price=int(int(obj['price']) / 2),
            pic=pic,
            visa_info=visa_info
        )
        return obj