import pickle 
from sorters import PriceSorter, AnalyticsSorter

def remove_extra_data(tours):
    for tour in tours:
        tour.pop('pics')
        tour.pop('departure')
        tour.pop('url')
    return tours

data = pickle.load(open('tours.p', 'rb'))
data = remove_extra_data(data)
data = AnalyticsSorter().sort(data)
import json
open('tours.json', 'w').write(json.dumps(data))
print(data[0])