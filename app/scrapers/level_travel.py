from datetime import datetime
from time import sleep
import re
from selenium.common.exceptions import StaleElementReferenceException
from countries import COUNTRIES
import linecache
import sys

def PrintException():
    exc_type, exc_obj, tb = sys.exc_info()
    f = tb.tb_frame
    lineno = tb.tb_lineno
    filename = f.f_code.co_filename
    linecache.checkcache(filename)
    line = linecache.getline(filename, lineno, f.f_globals)
    print('EXCEPTION IN ({}, LINE {} "{}"): {}'.format(filename, lineno, line.strip(), exc_obj))



COUNTRIES_MAP = {
    COUNTRIES.BULGARIA: 'BG',
    COUNTRIES.CYPRIS: 'CY',
    COUNTRIES.CZECH_REPUBLIC: 'CZ',
    COUNTRIES.FRANCE: 'FR',
    COUNTRIES.GREECE: 'GR',
    COUNTRIES.ISRAEL: 'IL',
    COUNTRIES.ITALY: 'IT',
    COUNTRIES.MONTENEGRO: 'ME',
    COUNTRIES.LATVIA: 'LV',
    COUNTRIES.SEYCHELLES: 'SC',
    COUNTRIES.SPAIN: 'ES',
    COUNTRIES.SRI_LANKA: 'LK',
    COUNTRIES.THAILAND: 'TH',
    COUNTRIES.TUNISIA: 'TN',
    COUNTRIES.TURKEY: 'TR',
    COUNTRIES.UAE: 'AE',
    COUNTRIES.VIETNAM: 'VN',
}

class LevelTravelScraper:
    def __init__(self, driver):
        self.driver = driver

    def _gen_search_url(self, country, departure, duration, adults, min_stars, max_stars):
        URL_TEMPLATE = "https://level.travel/search/Moscow-RU-to-Any-{country}-departure-{departure}-for-{duration}-nights-{adults}-adults-0-kids-{min_stars}..{max_stars}-stars"
        search_url = URL_TEMPLATE.format(
            country=country, 
            departure=departure, 
            duration=duration, 
            adults=adults, 
            min_stars=min_stars, 
            max_stars=max_stars
        )
        return search_url

    def _gen_hot_url(self, country, start_month=None):
        URL_TEMPLATE = "https://api.level.travel/widgets/monthly_stats.html?key=69d5cd04c4e8adbb71ba58af14558c2b&api_version=2&js=true&country_to={country}&city_select=true&&start_month={start_month}"
        if start_month is None:
            start_month = datetime.now().month
        hot_url = URL_TEMPLATE.format(
            country=country,
            start_month=start_month
        )
        return hot_url

    def _scrape_search_page(self, search_url, hotels_limit=1):
        LOADING_DELAY = 1
        LOADING_TIMEOUT = 30

        self.driver.get(search_url)
        MAX_ATTEMPTS = 3
        attempts = 0
        loading_finished = len(self.driver.find_elements_by_css_selector('#search_results.empty')) == 0
        while attempts < MAX_ATTEMPTS and not loading_finished:
            total_delay = 0
            while (not loading_finished) and (total_delay < LOADING_TIMEOUT):
                sleep(LOADING_DELAY)
                loading_finished = len(self.driver.find_elements_by_css_selector('#search_results.empty')) == 0
                total_delay += LOADING_DELAY
            if not loading_finished:
                print('Loading didn\'t finish, retrying')
                self.driver.refresh()
                attempts += 1
        if not loading_finished:
            print('Failed to scrape page, didn\'t load: {}'.format(search_url))
            return []
        results = []
        hotel_links = []
        for select_el in self.driver.find_elements_by_css_selector('.select_button'):
            if len(hotel_links) > hotels_limit:
                break
            hotel_links.append(select_el.get_attribute('href'))
        
        for hotel_link in hotel_links:
            try:
                self.driver.get(hotel_link)
                loading_finished = len(self.driver.find_elements_by_css_selector('.lt-hotel-results.ready')) > 0 and len(self.driver.find_elements_by_css_selector('li.lt-hotel-date.active')) > 0
                total_delay = 0
                while (not loading_finished) and (total_delay < LOADING_TIMEOUT):
                    sleep(LOADING_DELAY)
                    loading_finished = len(self.driver.find_elements_by_css_selector('.lt-hotel-results.ready')) > 0 and len(self.driver.find_elements_by_css_selector('li.lt-hotel-date.active')) > 0
                    total_delay += LOADING_DELAY
                if not loading_finished:
                    continue

                city_name = self.driver.find_elements_by_css_selector('.hotel-breadcrumb > ul > li:last-child')
                if len(city_name) > 0:
                    city_name = city_name[0].text
                else:
                    city_name = None
                hotel_name = self.driver.find_elements_by_css_selector('.hotel-name-value')[0].text
                stars = len(self.driver.find_elements_by_css_selector('.active.icon-smd-star'))

                departure = self.driver.find_elements_by_css_selector('li.lt-hotel-date.active')[0].get_attribute('data-reactid')
                departure = departure.split('$date-')[1]
                departure = datetime.strptime(departure, "%Y-%m-%d")

                # Фотографии отеля
                MAX_PICS = 3
                pics = []
                hotel_pics = self.driver.find_elements_by_css_selector('.rtee-images-item img')
                for pic in hotel_pics:
                    pics.append(pic.get_attribute('src'))

                MAX_ATTEMPTS = 3
                attempts = 0
                rooms_scraped = False
                while attempts < MAX_ATTEMPTS and not rooms_scraped:
                    try:
                        for result_body in self.driver.find_elements_by_css_selector('.result-body'):
                            room_name = result_body.find_elements_by_css_selector('.room-name')[0].text
                            for result_offer in result_body.find_elements_by_css_selector('.result-offer'):
                                pansion = result_offer.find_elements_by_css_selector('.pansion-name > em')[0].text
                                price = result_offer.find_elements_by_css_selector('.pansion-price-content')[0].text
                                price = float(re.sub("[^0-9]", "", price))
                                duration = result_offer.find_elements_by_css_selector('.pansion-nights > span')[0].text
                                duration = int(duration)
                                results.append({
                                    'city': city_name,
                                    'departure': departure,
                                    'url': hotel_link,
                                    'hotel_name': hotel_name,
                                    'nights': duration,
                                    'stars': stars,
                                    'room_name': room_name,
                                    'pansion': pansion,
                                    'price': price,
                                    'pics': pics
                                })
                        rooms_scraped = True
                    except:
                        print('Stale element referenced, retrying')
                        attempts += 1

                        self.driver.refresh()
                        sleep(20)
                if not rooms_scraped:
                    print('Failed to parse the page, max attempts count exceeded: {}'.format(hotel_link))
            except Exception as e:
                print('Failed to parse the page: {}, {}'.format(e, hotel_link))
                PrintException()
        return results

    def get_hot_tours(self, country, start_month=None):
        mapped_country = COUNTRIES_MAP[country]
        hot_url = self._gen_hot_url(mapped_country, start_month)
        self.driver.get(hot_url)
        search_urls = []
        rows = self.driver.find_elements_by_tag_name('tr')
        for row in rows[1:]:
            cells = row.find_elements_by_tag_name('td')
            for cell in cells[1:-1]:
                if len(cell.find_elements_by_tag_name('a')) > 0: 
                    link = cell.find_elements_by_tag_name('a')[0].get_attribute('href')
                    search_urls.append(link)
        results = []
        for url in search_urls:
            results += self._scrape_search_page(url)
        for result in results:
            result['destination'] = country
        return results

    def get_tours(self, country, departure, duration=7, adults=2, min_stars=0, max_stars=5):
        mapped_country = COUNTRIES_MAP[country]
        search_url = self._gen_search_url(mapped_country, departure, duration, adults, min_stars, max_stars)
        return self._scrape_search_page(search_url)
