import time
from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import pickle


from config import COUNTRIES, SCRAPERS, SORTERS, REDUCERS, PROCESSORS, POSTERS, LOAD_FROM_FILE

if __name__ == '__main__':
    start = time.time()

    if not LOAD_FROM_FILE:
        driver = webdriver.Remote(command_executor='http://selenium-chrome:4444/wd/hub',
                  desired_capabilities=DesiredCapabilities.CHROME)

        data = []

        for country in COUNTRIES:
            for scraper_class in SCRAPERS:
                data += scraper_class(driver).get_hot_tours(country)

        # Dump data to pickle file
        pickle.dump(data, open('./data/hot-tours-latest.p', 'wb'))
    else:
        # Loading data from test pickle file
        data = pickle.load(open('./data/hot-tours-latest.p', 'rb'))

    scraping_done = time.time()

    for sorter in SORTERS:
        data = sorter.sort(data)

    sorting_done = time.time()

    for reducer in REDUCERS:
        data = reducer.reduce(data)
        
    reducing_done = time.time()

    for processor in PROCESSORS:
        if isinstance(data, list):
            for i, datum in enumerate(data):
                data[i] = processor.process(datum)
        else:
            data = processor.process(data)

    processing_done = time.time()

    for poster in POSTERS:
        poster.post(data)

    posting_done = time.time()

    end = time.time()

    # Time calculation
    total_time = end - start
    scraping_time = scraping_done - start
    sorting_time = sorting_done - scraping_done
    reducing_time = reducing_done - sorting_done
    processing_time = processing_done - reducing_done
    posting_time = posting_done - processing_done

    print('''
        Execution completed.
        Total time: {}s
        Scraping time: {}s
        Sorting time: {}s
        Reducing time: {}s
        Processing time: {}s
        Posting time: {}s
    '''.format(
        total_time,
        scraping_time,
        sorting_time,
        reducing_time,
        processing_time,
        posting_time
    ))
