from scrapers import LevelTravelScraper
from sorters import AnalyticsSorter
from reducers import FirstNReducer, RandomChoiceReducer, DbDuplicatesRemover
from processors import GooglShortenerProcessor, TravelPayoutsProcessor, TourToMsgProcessor, IdGeneratorProcessor, CollageGeneratorProcessor
from posters import TelegramChannelPoster, DbPoster
from countries import COUNTRIES


LOAD_FROM_FILE = False
COUNTRIES = [
    COUNTRIES.THAILAND,
    COUNTRIES.CYPRIS,
    COUNTRIES.MONTENEGRO,
    COUNTRIES.BULGARIA,
    COUNTRIES.GREECE,
    COUNTRIES.TURKEY,
    COUNTRIES.UAE,
    COUNTRIES.SPAIN,
    COUNTRIES.ITALY,
    COUNTRIES.CZECH_REPUBLIC,
    COUNTRIES.TUNISIA,
    COUNTRIES.SRI_LANKA,
]
SCRAPERS = [LevelTravelScraper]
SORTERS = [AnalyticsSorter()]
REDUCERS = [IdGeneratorProcessor(), DbDuplicatesRemover(), FirstNReducer(1), RandomChoiceReducer()]
PROCESSORS = [
    CollageGeneratorProcessor(),
    TravelPayoutsProcessor('141615'),
    GooglShortenerProcessor('AIzaSyBYbyliLtLwvs7_j8lyzaCzKmhD_yUjQ0o'),
    TourToMsgProcessor()
]
POSTERS = [
    TelegramChannelPoster(
        chat_id='@travelbeavermsk',
        bot_token='392246475:AAFIbXrpBJkL01XV3DExcjCbfIapfAo9qBU'
    ),
    DbPoster()
]